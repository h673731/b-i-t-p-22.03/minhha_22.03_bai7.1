package com.example.minhha_22._bai71;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/baitap7.1")

public class Controller {
    @Autowired
    Service service;

    @GetMapping("/signIn")
    public ResponseEntity<?> signIn (@RequestParam String userName, @RequestParam String password) {
        return new ResponseEntity<>(service.signIn(userName, password), HttpStatus.OK);
    }

    @GetMapping("/checkToken")
    public ResponseEntity<?> checkToken (@RequestParam String token) {
        return new ResponseEntity<>(service.checkToken(token), HttpStatus.OK);
    }
}
