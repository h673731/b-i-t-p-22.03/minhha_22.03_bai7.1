package com.example.minhha_22._bai71;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SingletonArrayList {
    private static SingletonArrayList instance;
    private ArrayList<String> arrayList;
    private Timer timer;

    private SingletonArrayList() {
        arrayList = new ArrayList<>();
        // Khởi tạo timer để lên lịch loại bỏ phần tử từ ArrayList mỗi 1 phút
        timer = new Timer();
        // Lên lịch cho nhiệm vụ loại bỏ mỗi 1 phút
        timer.schedule(new RemoveElementTask(), 0, 60000);
    }

    public static SingletonArrayList getInstance() {
        if (instance == null) {
            instance = new SingletonArrayList();
        }
        return instance;
    }

    public void addElement(String element) {
        arrayList.add(element);
    }

    public ArrayList<String> getArrayList() {
        return arrayList;
    }

    private class RemoveElementTask extends TimerTask {
        @Override
        public void run() {
            // Loại bỏ phần tử đầu tiên của ArrayList (nếu có)
            if (arrayList.size() == 0) {
                System.out.println("Không có token!");
                return;
            }
            for (int i = 0; i < arrayList.size(); i++ ) {
                String [] parts = arrayList.get(i).split("_");
                long expirationTimeMillis = Long.parseLong(parts[1]);
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis > expirationTimeMillis) {
                    arrayList.remove(i);
                    break;
                }
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(System.currentTimeMillis());
    }

}
