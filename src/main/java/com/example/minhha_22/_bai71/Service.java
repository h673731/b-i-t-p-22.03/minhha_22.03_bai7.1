package com.example.minhha_22._bai71;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.UUID;

@Component
public class Service {
    private final String userName = "admin";
    private final String password = "123456";

    SingletonArrayList singletonArraylist = SingletonArrayList.getInstance();


    private String generateToken () {
        long expirationTime = System.currentTimeMillis() + 60 * 1000;
        return UUID.randomUUID().toString() + "_" + expirationTime;
    }

    public ArrayList<String> signIn(String userName, String password){
        if (userName.equals(this.userName) && password.equals(this.password)){
            singletonArraylist.addElement(generateToken());
            return singletonArraylist.getArrayList();
        }
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Đăng nhập thất bại!");
        return arrayList;
    }

    public String checkToken (String token) {
        for (String string : singletonArraylist.getArrayList()) {
            if (token.equals(string)){
                String [] parts = string.split("_");
                long expirationTimeMillis = Long.parseLong(parts[1]);
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis < expirationTimeMillis){
                    return "Token còn hạn!";
                }
                return "Token đã hết hạn!";
            }
        }
        return "Token không tồn tại!";
    }

}
